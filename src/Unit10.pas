unit Unit10;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Tvelocidade = class(TForm)
    RadioGroup1: TRadioGroup;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  velocidade: Tvelocidade;
  veloc:double;

implementation

{$R *.DFM}

procedure atualiza;
begin
if velocidade.radiobutton1.checked then begin
    velocidade.panel1.caption:=floattostr(veloc)+' m/s ';
    velocidade.panel2.caption:=floattostr(veloc*3.6)+' km/h ';
    velocidade.panel6.caption:=floattostr(veloc*2.236936292)+' mph ';
    velocidade.panel7.caption:=floattostr(veloc*0.003016955)+' mach ';
    end;

if velocidade.radiobutton2.checked then begin
    velocidade.panel1.caption:=floattostr(veloc*0.277777778)+' m/s ';
    velocidade.panel2.caption:=floattostr(veloc*1)+' km/h ';
    velocidade.panel6.caption:=floattostr(veloc*0.621371192)+' mph ';
    velocidade.panel7.caption:=floattostr(veloc*0.000840579)+' mach ';
    end;

if velocidade.radiobutton3.checked then begin
    velocidade.panel1.caption:=floattostr(veloc*0.44704)+' m/s ';
    velocidade.panel2.caption:=floattostr(veloc*1.609344)+' km/h ';
    velocidade.panel6.caption:=floattostr(veloc*1)+' mph ';
    velocidade.panel7.caption:=floattostr(veloc*0.001352781)+' mach ';
    end;

if velocidade.radiobutton4.checked then begin
    velocidade.panel1.caption:=floattostr(veloc*331.46)+' m/s ';
    velocidade.panel2.caption:=floattostr(veloc*1189.656)+' km/h ';
    velocidade.panel6.caption:=floattostr(veloc*739.2179671)+' mph ';
    velocidade.panel7.caption:=floattostr(veloc*1)+' mach ';
    end;

    end;



procedure Tvelocidade.Edit1Exit(Sender: TObject);
begin
veloc:=strtofloat(edit1.text);
atualiza;
end;

procedure Tvelocidade.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tvelocidade.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tvelocidade.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure Tvelocidade.RadioButton4Click(Sender: TObject);
begin
atualiza;
end;

procedure Tvelocidade.Image1Click(Sender: TObject);
begin
veloc:=veloc-1;
edit1.text:=floattostr(veloc);
atualiza;

end;

procedure Tvelocidade.Image2Click(Sender: TObject);
begin
veloc:=veloc+1;
edit1.text:=floattostr(veloc);
atualiza;

end;

end.
