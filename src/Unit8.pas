unit Unit8;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  Ttempo = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel10: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel2: TPanel;
    RadioGroup1: TRadioGroup;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton8: TRadioButton;
    RadioButton9: TRadioButton;
    RadioButton11: TRadioButton;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure RadioButton8Click(Sender: TObject);
    procedure RadioButton9Click(Sender: TObject);
    procedure RadioButton11Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  tempo: Ttempo;
  time: double;

implementation

{$R *.DFM}

procedure atualiza;
begin
if tempo.radiobutton1.checked then begin
    tempo.panel1.caption:=floattostr(time)+' s ';
    tempo.panel2.caption:=floattostr(time*0.016666667)+' min ';
    tempo.panel6.caption:=floattostr(time*0.000277778)+' h ';
    tempo.panel7.caption:=floattostr(time*1.16E-05)+' dias ';
    tempo.panel10.caption:=floattostr(time*1.65E-06)+' semanas ';
    tempo.panel8.caption:=floattostr(time*3.80E-07)+' meses ';
    tempo.panel12.caption:=floattostr(time*3.17E-08)+' anos ';
    tempo.panel13.caption:=floattostr(time*3.17E-10)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*3.17E-11)+' mil�nios ';
    end;

if tempo.radiobutton2.checked then begin
    tempo.panel1.caption:=floattostr(time*60)+' s ';
    tempo.panel2.caption:=floattostr(time*1)+' min ';
    tempo.panel6.caption:=floattostr(time*0.166666667)+' h ';
    tempo.panel7.caption:=floattostr(time*0.000694444)+' dias ';
    tempo.panel10.caption:=floattostr(time*9.92E-05)+' semanas ';
    tempo.panel8.caption:=floattostr(time*2.28E-05)+' meses ';
    tempo.panel12.caption:=floattostr(time*1.90E-06)+' anos ';
    tempo.panel13.caption:=floattostr(time*1.90E-08)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*1.90E-09)+' mil�nios ';
    end;

if tempo.radiobutton3.checked then begin
    tempo.panel1.caption:=floattostr(time*3600)+' s ';
    tempo.panel2.caption:=floattostr(time*60)+' min ';
    tempo.panel6.caption:=floattostr(time*1)+' h ';
    tempo.panel7.caption:=floattostr(time*0.041666667)+' dias ';
    tempo.panel10.caption:=floattostr(time*0.005952381)+' semanas ';
    tempo.panel8.caption:=floattostr(time*0.001368925)+' meses ';
    tempo.panel12.caption:=floattostr(time*0.000114077)+' anos ';
    tempo.panel13.caption:=floattostr(time*1.14E-06)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*1.14E-07)+' mil�nios ';
    end;

if tempo.radiobutton4.checked then begin
    tempo.panel1.caption:=floattostr(time*86400)+' s ';
    tempo.panel2.caption:=floattostr(time*1440)+' min ';
    tempo.panel6.caption:=floattostr(time*24)+' h ';
    tempo.panel7.caption:=floattostr(time*1)+' dias ';
    tempo.panel10.caption:=floattostr(time*0.142857143)+' semanas ';
    tempo.panel8.caption:=floattostr(time*0.032854209)+' meses ';
    tempo.panel12.caption:=floattostr(time*0.002737851)+' anos ';
    tempo.panel13.caption:=floattostr(time*2.74E-05)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*2.74E-06)+' mil�nios ';
    end;

if tempo.radiobutton5.checked then begin
    tempo.panel1.caption:=floattostr(time*604800)+' s ';
    tempo.panel2.caption:=floattostr(time*10080)+' min ';
    tempo.panel6.caption:=floattostr(time*168)+' h ';
    tempo.panel7.caption:=floattostr(time*7)+' dias ';
    tempo.panel10.caption:=floattostr(time*1)+' semanas ';
    tempo.panel8.caption:=floattostr(time*0.229979466)+' meses ';
    tempo.panel12.caption:=floattostr(time*0.019164956)+' anos ';
    tempo.panel13.caption:=floattostr(time*0.00019165)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*1.92E-05)+' mil�nios ';
    end;

if tempo.radiobutton6.checked then begin
    tempo.panel1.caption:=floattostr(time*2629800)+' s ';
    tempo.panel2.caption:=floattostr(time*43830)+' min ';
    tempo.panel6.caption:=floattostr(time*730.5)+' h ';
    tempo.panel7.caption:=floattostr(time*30.4375)+' dias ';
    tempo.panel10.caption:=floattostr(time*4.348214286)+' semanas ';
    tempo.panel8.caption:=floattostr(time*1)+' meses ';
    tempo.panel12.caption:=floattostr(time*0.083333333)+' anos ';
    tempo.panel13.caption:=floattostr(time*0.000833333)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*0.349833333)+' mil�nios ';
    end;

if tempo.radiobutton8.checked then begin
    tempo.panel1.caption:=floattostr(time*31557600)+' s ';
    tempo.panel2.caption:=floattostr(time*525960)+' min ';
    tempo.panel6.caption:=floattostr(time*8766)+' h ';
    tempo.panel7.caption:=floattostr(time*365.25)+' dias ';
    tempo.panel10.caption:=floattostr(time*52.17857143)+' semanas ';
    tempo.panel8.caption:=floattostr(time*12)+' meses ';
    tempo.panel12.caption:=floattostr(time*1)+' anos ';
    tempo.panel13.caption:=floattostr(time*0.01)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*0.001)+' mil�nios ';
    end;

if tempo.radiobutton9.checked then begin
    tempo.panel1.caption:=floattostr(time*315576E+04)+' s ';
    tempo.panel2.caption:=floattostr(time*52596000)+' min ';
    tempo.panel6.caption:=floattostr(time*876600)+' h ';
    tempo.panel7.caption:=floattostr(time*36525)+' dias ';
    tempo.panel10.caption:=floattostr(time*5217.857143)+' semanas ';
    tempo.panel8.caption:=floattostr(time*1200)+' meses ';
    tempo.panel12.caption:=floattostr(time*100)+' anos ';
    tempo.panel13.caption:=floattostr(time*1)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*0.1)+' mil�nios ';
    end;

if tempo.radiobutton11.checked then begin
    tempo.panel1.caption:=floattostr(time*3.16E+15)+' s ';
    tempo.panel2.caption:=floattostr(time*525960000)+' min ';
    tempo.panel6.caption:=floattostr(time*8766000)+' h ';
    tempo.panel7.caption:=floattostr(time*365250)+' dias ';
    tempo.panel10.caption:=floattostr(time*52178.57143)+' semanas ';
    tempo.panel8.caption:=floattostr(time*12000)+' meses ';
    tempo.panel12.caption:=floattostr(time*1000)+' anos ';
    tempo.panel13.caption:=floattostr(time*10)+' s�culos ';
    tempo.panel14.caption:=floattostr(time*1)+' mil�nios ';
    end;


end;

procedure Ttempo.Edit1Exit(Sender: TObject);
begin
time:=strtofloat(edit1.text);
atualiza;
end;

procedure Ttempo.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.RadioButton4Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.RadioButton5Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.RadioButton6Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.RadioButton8Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.RadioButton9Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.RadioButton11Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttempo.Image1Click(Sender: TObject);
begin
time:=time-1;
edit1.text:=floattostr(time);
atualiza;

end;

procedure Ttempo.Image2Click(Sender: TObject);
begin
time:=time+1;
edit1.text:=floattostr(time);
atualiza;

end;

end.
