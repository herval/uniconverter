unit Unit7;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Tenergia = class(TForm)
    GroupBox2: TGroupBox;
    RadioGroup1: TRadioGroup;
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    Image2: TImage;
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  energia: Tenergia;
  energy:double;

implementation

{$R *.DFM}

procedure atualiza;
begin
if energia.radiobutton1.checked then begin
   energia.panel1.caption:=floattostr(energy*1)+' J ';
   energia.panel2.caption:=floattostr(energy*10000000)+' Erg ';
   end;

if energia.radiobutton2.checked then begin
   energia.panel1.caption:=floattostr(energy*1.00E-07)+' J ';
   energia.panel2.caption:=floattostr(energy*1)+' Erg ';
   end;

   end;

procedure Tenergia.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tenergia.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tenergia.Edit1Exit(Sender: TObject);
begin
energy:=strtofloat(edit1.text);

atualiza;
end;

procedure Tenergia.Image1Click(Sender: TObject);
begin
energy:=energy-1;
edit1.text:=floattostr(energy);
atualiza;

end;

procedure Tenergia.Image2Click(Sender: TObject);
begin
energy:=energy+1;
edit1.text:=floattostr(energy);
atualiza;

end;

end.
