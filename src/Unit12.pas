unit Unit12;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Tpotencia = class(TForm)
    RadioGroup1: TRadioGroup;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel2: TPanel;
    RadioButton5: TRadioButton;
    Panel3: TPanel;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  potencia: Tpotencia;
  pot:double;

implementation

{$R *.DFM}

procedure atualiza;
begin
if potencia.radiobutton1.checked then begin
    potencia.panel1.caption:=floattostr(pot)+' W ';
    potencia.panel2.caption:=floattostr(pot*0.001)+' qW ';
    potencia.panel6.caption:=floattostr(pot*1.00E-06)+' mW ';
    potencia.panel7.caption:=floattostr(pot*0.001359622)+' Hp ';
    potencia.panel3.caption:=floattostr(pot*859.8452279)+' cal/h ';
    end;

if potencia.radiobutton2.checked then begin
    potencia.panel1.caption:=floattostr(pot*1000)+' W ';
    potencia.panel2.caption:=floattostr(pot*1)+' qW ';
    potencia.panel6.caption:=floattostr(pot*0.001)+' mW ';
    potencia.panel7.caption:=floattostr(pot*1.359621617)+' Hp ';
    potencia.panel3.caption:=floattostr(pot*859845.2279)+' cal/h ';
    end;

if potencia.radiobutton3.checked then begin
    potencia.panel1.caption:=floattostr(pot*1000000)+' W ';
    potencia.panel2.caption:=floattostr(pot*1000)+' qW ';
    potencia.panel6.caption:=floattostr(pot*1)+' mW ';
    potencia.panel7.caption:=floattostr(pot*1359.621617)+' Hp ';
    potencia.panel3.caption:=floattostr(pot*859845227.9)+' cal/h ';
    end;

if potencia.radiobutton4.checked then begin
    potencia.panel1.caption:=floattostr(pot*735.49875)+' W ';
    potencia.panel2.caption:=floattostr(pot*0.73549875)+' qW ';
    potencia.panel6.caption:=floattostr(pot*0.000735499)+' mW ';
    potencia.panel7.caption:=floattostr(pot*1)+' Hp ';
    potencia.panel3.caption:=floattostr(pot*632415.0903)+' cal/h ';
    end;

if potencia.radiobutton5.checked then begin
    potencia.panel1.caption:=floattostr(pot*1.001163)+' W ';
    potencia.panel2.caption:=floattostr(pot*1.16E-06)+' qW ';
    potencia.panel6.caption:=floattostr(pot*1.16E-09)+' mW ';
    potencia.panel7.caption:=floattostr(pot*1.58E-06)+' Hp ';
    potencia.panel3.caption:=floattostr(pot*1)+' cal/h ';
    end;

    end;



procedure Tpotencia.Edit1Exit(Sender: TObject);
begin
pot:=strtofloat(edit1.text);
atualiza;
end;



procedure Tpotencia.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpotencia.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpotencia.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpotencia.RadioButton4Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpotencia.RadioButton5Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpotencia.Image1Click(Sender: TObject);
begin
pot:=pot-1;
edit1.text:=floattostr(pot);
atualiza;

end;

procedure Tpotencia.Image2Click(Sender: TObject);
begin
pot:=pot+1;
edit1.text:=floattostr(pot);
atualiza;

end;

end.
