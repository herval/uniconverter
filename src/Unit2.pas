unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Ttemp = class(TForm)
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);

    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  temp: Ttemp;
  tempera:real;

implementation

{$R *.DFM}

procedure atualiza;
begin
if temp.radiobutton1.Checked then begin
   temp.panel1.caption:=temp.edit1.text+' �C ';
   temp.panel2.caption:=floattostr(9*(tempera/5)+32)+' �F';
   temp.panel3.caption:=floattostr(tempera+273)+' K';
   end;
if temp.radiobutton2.checked then begin
   temp.panel1.caption:=floattostr(((tempera-32)*5)/9)+' �C';
   temp.panel2.caption:=temp.edit1.text+' �F ';
   temp.panel3.caption:=floattostr((((tempera-32)*5)/9)+273)+' K';
   end;
if temp.radiobutton3.checked then begin
   temp.panel1.caption:=floattostr(tempera-273)+' �C';
   temp.panel2.caption:=floattostr((9*(tempera-273)/5)+32)+' �F';
   temp.panel3.caption:=temp.edit1.text+' K ';
   end;
end;


procedure Ttemp.Edit1Exit(Sender: TObject);
begin
tempera:=strtofloat(edit1.text);
atualiza;
end;

procedure Ttemp.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttemp.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Ttemp.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;




procedure Ttemp.Image1Click(Sender: TObject);
begin
tempera:=tempera-1;
edit1.text:=floattostr(tempera);
atualiza;
end;

procedure Ttemp.Image2Click(Sender: TObject);
begin
tempera:=tempera+1;
edit1.text:=floattostr(tempera);
atualiza;
end;

end.
