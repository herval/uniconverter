unit Unit16;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, MPlayer;

type
  TAboutBox1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    MediaPlayer1: TMediaPlayer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox1: TAboutBox1;

implementation

{$R *.DFM}


procedure TAboutBox1.Timer1Timer(Sender: TObject);
begin
aboutbox1.close;
timer1.Enabled:=false;
end;

procedure TAboutBox1.FormShow(Sender: TObject);
begin
mediaplayer1.play;
end;

procedure TAboutBox1.Image1Click(Sender: TObject);
begin
aboutbox1.close;
end;

end.
 
