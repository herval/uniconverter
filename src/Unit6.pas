unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForca = class(TForm)
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Change(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Forca: TForca;
  force:double;

implementation

{$R *.DFM}

procedure atualiza;
begin
if forca.radiobutton1.checked then begin
   forca.panel1.caption:=floattostr(force*1)+' dyn ';
   forca.panel2.caption:=floattostr(force*1.00E-05)+' N ';
   forca.panel3.caption:=floattostr(force*0.001019716213*1000)+' Kgf ';
   end;

if forca.radiobutton2.checked then begin
   forca.panel1.caption:=floattostr(force*100000)+' dyn ';
   forca.panel2.caption:=floattostr(force*1)+' N ';
   forca.panel3.caption:=floattostr(force*1000*101.9716213)+' Kgf ';
   end;

if forca.radiobutton3.checked then begin
   forca.panel1.caption:=floattostr(force*980.665/1000)+' dyn ';
   forca.panel2.caption:=floattostr(force*0.00980665/1000)+' N ';
   forca.panel3.caption:=floattostr(force*1)+' Kgf ';
   end;
end;

procedure TForca.Edit1Change(Sender: TObject);
begin
force:=strtofloat(edit1.text);
atualiza;
end;

procedure TForca.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure TForca.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure TForca.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure TForca.Image1Click(Sender: TObject);
begin
force:=force-1;
edit1.text:=floattostr(force);
atualiza;

end;

procedure TForca.Image2Click(Sender: TObject);
begin
force:=force+1;
edit1.text:=floattostr(force);
atualiza;

end;

end.
