unit Unit13;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Tarea = class(TForm)
    RadioGroup1: TRadioGroup;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel8: TPanel;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure RadioButton7Click(Sender: TObject);
    procedure RadioButton8Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  area: Tarea;
  areah: double;

implementation

{$R *.DFM}

procedure atualiza;
begin
if area.radiobutton1.checked then begin
    area.panel1.caption:=floattostr(areah)+' mm� ';
    area.panel2.caption:=floattostr(areah*0.01)+' cm� ';
    area.panel6.caption:=floattostr(areah*1.00E-06)+' m� ';
    area.panel7.caption:=floattostr(areah*1.00E-08)+' ares ';
    area.panel3.caption:=floattostr(areah*1.00E-10)+' hec ';
    area.panel8.caption:=floattostr(areah*1.00E-12)+' km� ';
    area.panel4.caption:=floattostr(areah*2.47E-10)+' acres ';
    area.panel5.caption:=floattostr(areah*3.86E-13)+' mi� ';
    end;

if area.radiobutton2.checked then begin
    area.panel1.caption:=floattostr(areah*100)+' mm� ';
    area.panel2.caption:=floattostr(areah*1)+' cm� ';
    area.panel6.caption:=floattostr(areah*0.0001)+' m� ';
    area.panel7.caption:=floattostr(areah*1.00E-06)+' ares ';
    area.panel3.caption:=floattostr(areah*1.00E-08)+' hec ';
    area.panel8.caption:=floattostr(areah*1.00E-10)+' km� ';
    area.panel4.caption:=floattostr(areah*2.47E-08)+' acres ';
    area.panel5.caption:=floattostr(areah*3.86E-11)+' mi� ';
    end;

if area.radiobutton3.checked then begin
    area.panel1.caption:=floattostr(areah*1000000)+' mm� ';
    area.panel2.caption:=floattostr(areah*10000)+' cm� ';
    area.panel6.caption:=floattostr(areah*1)+' m� ';
    area.panel7.caption:=floattostr(areah*0.01)+' ares ';
    area.panel3.caption:=floattostr(areah*0.0001)+' hec ';
    area.panel8.caption:=floattostr(areah*1.00E-06)+' km� ';
    area.panel4.caption:=floattostr(areah*0.000247105)+' acres ';
    area.panel5.caption:=floattostr(areah*3.86E-07)+' mi� ';
    end;

if area.radiobutton4.checked then begin
    area.panel1.caption:=floattostr(areah*100000000)+' mm� ';
    area.panel2.caption:=floattostr(areah*1000000)+' cm� ';
    area.panel6.caption:=floattostr(areah*100)+' m� ';
    area.panel7.caption:=floattostr(areah*1)+' ares ';
    area.panel3.caption:=floattostr(areah*0.01)+' hec ';
    area.panel8.caption:=floattostr(areah*0.0001)+' km� ';
    area.panel4.caption:=floattostr(areah*0.024710538)+' acres ';
    area.panel5.caption:=floattostr(areah*3.86E-05)+' mi� ';
    end;

if area.radiobutton5.checked then begin
    area.panel1.caption:=floattostr(areah*1.00E+10)+' mm� ';
    area.panel2.caption:=floattostr(areah*100000000)+' cm� ';
    area.panel6.caption:=floattostr(areah*10000)+' m� ';
    area.panel7.caption:=floattostr(areah*100)+' ares ';
    area.panel3.caption:=floattostr(areah*1)+' hec ';
    area.panel8.caption:=floattostr(areah*0.01)+' km� ';
    area.panel4.caption:=floattostr(areah*2.47105381)+' acres ';
    area.panel5.caption:=floattostr(areah*3.86E-03)+' mi� ';
    end;

if area.radiobutton6.checked then begin
    area.panel1.caption:=floattostr(areah*1.00E+12)+' mm� ';
    area.panel2.caption:=floattostr(areah*1.00E+10)+' cm� ';
    area.panel6.caption:=floattostr(areah*1000000)+' m� ';
    area.panel7.caption:=floattostr(areah*10000)+' ares ';
    area.panel3.caption:=floattostr(areah*100)+' hec ';
    area.panel8.caption:=floattostr(areah*1)+' km� ';
    area.panel4.caption:=floattostr(areah*247.105381)+' acres ';
    area.panel5.caption:=floattostr(areah*3.86E-01)+' mi� ';
    end;

if area.radiobutton7.checked then begin
    area.panel1.caption:=floattostr(areah*404685642E+01)+' mm� ';
    area.panel2.caption:=floattostr(areah*40468564.22)+' cm� ';
    area.panel6.caption:=floattostr(areah*4046.856422)+' m� ';
    area.panel7.caption:=floattostr(areah*40.46856422)+' ares ';
    area.panel3.caption:=floattostr(areah*0.404685642)+' hec ';
    area.panel8.caption:=floattostr(areah*0.004046856)+' km� ';
    area.panel4.caption:=floattostr(areah*1)+' acres ';
    area.panel5.caption:=floattostr(areah*0.0015625)+' mi� ';
    end;

if area.radiobutton8.checked then begin
    area.panel1.caption:=floattostr(areah*2.57E+12)+' mm� ';
    area.panel2.caption:=floattostr(areah*2.59E+10)+' cm� ';
    area.panel6.caption:=floattostr(areah*2589988.11)+' m� ';
    area.panel7.caption:=floattostr(areah*25899.8811)+' ares ';
    area.panel3.caption:=floattostr(areah*258.998811)+' hec ';
    area.panel8.caption:=floattostr(areah*2.58998811)+' km� ';
    area.panel4.caption:=floattostr(areah*640)+' acres ';
    area.panel5.caption:=floattostr(areah*1)+' mi� ';
    end;
    end;



procedure Tarea.Edit1Exit(Sender: TObject);
begin
areah:=strtofloat(edit1.text);
end;

procedure Tarea.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tarea.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tarea.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure Tarea.RadioButton4Click(Sender: TObject);
begin
atualiza;
end;

procedure Tarea.RadioButton5Click(Sender: TObject);
begin
atualiza;
end;

procedure Tarea.RadioButton6Click(Sender: TObject);
begin
atualiza;
end;

procedure Tarea.RadioButton7Click(Sender: TObject);
begin
atualiza;
end;

procedure Tarea.RadioButton8Click(Sender: TObject);
begin
atualiza;
end;

procedure Tarea.Image1Click(Sender: TObject);
begin
areah:=areah-1;
edit1.text:=floattostr(areah);
atualiza;

end;

procedure Tarea.Image2Click(Sender: TObject);
begin
areah:=areah+1;
edit1.text:=floattostr(areah);
atualiza;

end;

end.
