unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Tvolume = class(TForm)
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  volume: Tvolume;
  volum:double;

  implementation

{$R *.DFM}

procedure atualiza;
begin
if volume.radiobutton1.checked then begin
   volume.panel1.caption:=floattostr(volum*1)+' mL ';
   volume.panel2.caption:=floattostr(volum*0.001)+' L ';
   volume.panel3.caption:=floattostr(volum*1.00E-06)+' m� ';
   end;

if volume.radiobutton2.checked then begin
   volume.panel1.caption:=floattostr(volum*100)+' mL ';
   volume.panel2.caption:=floattostr(volum*1)+' L ';
   volume.panel3.caption:=floattostr(volum*0.001)+' m� ';
   end;

if volume.radiobutton3.checked then begin
   volume.panel1.caption:=floattostr(volum*1000000)+' mL ';
   volume.panel2.caption:=floattostr(volum*1000)+' L ';
   volume.panel3.caption:=floattostr(volum*1)+' m� ';
   end;
end;


procedure Tvolume.Edit1Exit(Sender: TObject);
begin
volum:=strtofloat(edit1.text);

atualiza;
end;

procedure Tvolume.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tvolume.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tvolume.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;


procedure Tvolume.Image1Click(Sender: TObject);
begin
volum:=volum-1;
edit1.text:=floattostr(volum);
atualiza;

end;

procedure Tvolume.Image2Click(Sender: TObject);
begin
volum:=volum+1;
edit1.text:=floattostr(volum);
atualiza;

end;

end.
