unit Unit9;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Tdensidade = class(TForm)
    RadioGroup1: TRadioGroup;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  densidade: Tdensidade;
  densi:double;

implementation

{$R *.DFM}

procedure atualiza;
begin
if densidade.radiobutton1.checked then begin
    densidade.panel1.caption:=floattostr(densi)+' kg/m� ';
    densidade.panel2.caption:=floattostr(densi*1000)+' g/m� ';
    densidade.panel6.caption:=floattostr(densi*0.006242796)+' lb/ft� ';
    densidade.panel7.caption:=floattostr(densi*3.61E-05)+' lb/pol� ';
    end;

if densidade.radiobutton2.checked then begin
    densidade.panel1.caption:=floattostr(densi*0.001)+' kg/m� ';
    densidade.panel2.caption:=floattostr(densi*1)+' g/m� ';
    densidade.panel6.caption:=floattostr(densi*62.42796055)+' lb/ft� ';
    densidade.panel7.caption:=floattostr(densi*3.61E-02)+' lb/pol� ';
    end;

if densidade.radiobutton3.checked then begin
    densidade.panel1.caption:=floattostr(densi*16.01846338)+' kg/m� ';
    densidade.panel2.caption:=floattostr(densi*0.016018463)+' g/m� ';
    densidade.panel6.caption:=floattostr(densi*1)+' lb/ft� ';
    densidade.panel7.caption:=floattostr(densi*0)+' lb/pol� ';
    end;

if densidade.radiobutton4.checked then begin
    densidade.panel1.caption:=floattostr(densi*27679.90471)+' kg/m� ';
    densidade.panel2.caption:=floattostr(densi*27.67990471)+' g/m� ';
    densidade.panel6.caption:=floattostr(densi*0)+' lb/ft� ';
    densidade.panel7.caption:=floattostr(densi*1)+' lb/pol� ';
    end;


end;

procedure Tdensidade.Edit1Exit(Sender: TObject);
begin
densi:=strtofloat(edit1.text);
atualiza;
end;

procedure Tdensidade.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdensidade.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdensidade.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdensidade.RadioButton4Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdensidade.Image1Click(Sender: TObject);
begin
densi:=densi-1;
edit1.text:=floattostr(densi);
atualiza;

end;

procedure Tdensidade.Image2Click(Sender: TObject);
begin
densi:=densi+1;
edit1.text:=floattostr(densi);
atualiza;

end;

end.
