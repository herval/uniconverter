unit Unit11;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Tpressao = class(TForm)
    RadioGroup1: TRadioGroup;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  pressao: Tpressao;
  press:double;

implementation

{$R *.DFM}
procedure atualiza;
begin
if pressao.radiobutton1.checked then begin
    pressao.panel1.caption:=floattostr(press)+' pa ';
    pressao.panel2.caption:=floattostr(press*1.00E-05)+' ba ';
    pressao.panel6.caption:=floattostr(press*9.87E-06)+' atm ';
    pressao.panel7.caption:=floattostr(press*0.007500617)+' mmHg ';
    end;

if pressao.radiobutton2.checked then begin
    pressao.panel1.caption:=floattostr(press*100000)+' pa ';
    pressao.panel2.caption:=floattostr(press*1)+' ba ';
    pressao.panel6.caption:=floattostr(press*0.986923267)+' atm ';
    pressao.panel7.caption:=floattostr(press*750.0616828)+' mmHg ';
    end;

if pressao.radiobutton3.checked then begin
    pressao.panel1.caption:=floattostr(press*101325)+' pa ';
    pressao.panel2.caption:=floattostr(press*1.01325)+' ba ';
    pressao.panel6.caption:=floattostr(press*1)+' atm ';
    pressao.panel7.caption:=floattostr(press*14.69594878)+' mmHg ';
    end;

if pressao.radiobutton4.checked then begin
    pressao.panel1.caption:=floattostr(press*133.3223684)+' pa ';
    pressao.panel2.caption:=floattostr(press*0.001333224)+' ba ';
    pressao.panel6.caption:=floattostr(press*0.001315789)+' atm ';
    pressao.panel7.caption:=floattostr(press*1)+' mmHg ';
    end;


end;

procedure Tpressao.Edit1Exit(Sender: TObject);
begin
press:=strtofloat(edit1.text);
atualiza;
end;

procedure Tpressao.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpressao.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpressao.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpressao.RadioButton4Click(Sender: TObject);
begin
atualiza;
end;

procedure Tpressao.Image1Click(Sender: TObject);
begin
press:=press-1;
edit1.text:=floattostr(press);
atualiza;

end;

procedure Tpressao.Image2Click(Sender: TObject);
begin
press:=press+1;
edit1.text:=floattostr(press);
atualiza;

end;

end.
