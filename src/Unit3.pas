unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  Tdist = class(TForm)
    Edit1: TEdit;
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton8: TRadioButton;
    RadioButton9: TRadioButton;
    RadioButton10: TRadioButton;
    RadioButton11: TRadioButton;
    RadioButton12: TRadioButton;
    RadioButton13: TRadioButton;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel10: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure RadioButton8Click(Sender: TObject);
    procedure RadioButton9Click(Sender: TObject);
    procedure RadioButton10Click(Sender: TObject);
    procedure RadioButton11Click(Sender: TObject);
    procedure RadioButton12Click(Sender: TObject);
    procedure RadioButton13Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dist: Tdist;
  dista:double;

implementation

{$R *.DFM}

procedure atualiza;
begin
if dist.radiobutton1.checked then begin
    dist.panel1.caption:=dist.edit1.text+' � ';
    dist.panel2.caption:=floattostr(dista*0.001)+' � ';
    dist.panel6.caption:=floattostr(dista*1e-07)+' mm ';
    dist.panel7.caption:=floattostr(dista*1e-08)+' cm ';
    dist.panel10.caption:=floattostr(dista*1e-10)+' m ';
    dist.panel8.caption:=floattostr(dista*1e-13)+' km ';
    dist.panel12.caption:=floattostr(dista*3.937007874e-09)+' pol ';
    dist.panel13.caption:=floattostr(dista*3.280839895e-10)+' p�s ';
    dist.panel14.caption:=floattostr(dista*1.093613298e-10)+' jardas ';
    dist.panel15.caption:=floattostr(dista*6.213711922e-14)+' mil ';
    dist.panel16.caption:=floattostr(dista*5.399568035e-14)+' mil ';
    dist.panel3.caption:=floattostr(dista*1.057020998e-26)+' al ';
    end;

if dist.radiobutton2.checked then begin
    dist.panel1.caption:=floattostr(1000*dista)+' � ';
    dist.panel2.caption:=dist.edit1.text+' � ';
    dist.panel6.caption:=floattostr(dista*0.001)+' mm ';
    dist.panel7.caption:=floattostr(dista*0.0001)+' cm ';
    dist.panel10.caption:=floattostr(dista*1e-06)+' m ';
    dist.panel8.caption:=floattostr(dista*1e-09)+' km ';
    dist.panel12.caption:=floattostr(dista*3.937007874e-05)+' pol ';
    dist.panel13.caption:=floattostr(dista*3.280839895e-06)+' p�s ';
    dist.panel14.caption:=floattostr(dista*1.093613298e-06)+' jardas ';
    dist.panel15.caption:=floattostr(dista*6.213711922e-10)+' mil ';
    dist.panel16.caption:=floattostr(dista*5.399568035e-10)+' mil ';
    dist.panel3.caption:=floattostr(dista*1.057020998e-22)+' al ';
    end;

if dist.radiobutton3.checked then begin
    dist.panel1.caption:=floattostr(10000000*dista)+' � ';
    dist.panel2.caption:=floattostr(dista*1000)+' � ';
    dist.panel6.caption:=dist.edit1.text+' mm ';
    dist.panel7.caption:=floattostr(dista*0.1)+' cm ';
    dist.panel10.caption:=floattostr(dista*0.001)+' m ';
    dist.panel8.caption:=floattostr(dista*1e-06)+' km ';
    dist.panel12.caption:=floattostr(dista*0.03937007874)+' pol ';
    dist.panel13.caption:=floattostr(dista*0.003280839895)+' p�s ';
    dist.panel14.caption:=floattostr(dista*0.001093613298)+' jardas ';
    dist.panel15.caption:=floattostr(dista*6.213711922e-07)+' mil ';
    dist.panel16.caption:=floattostr(dista*5.399568035e-07)+' mil ';
    dist.panel3.caption:=floattostr(dista*1.057020998e-19)+' al ';
    end;

if dist.radiobutton4.checked then begin
    dist.panel1.caption:=floattostr(100000000*dista)+' � ';
    dist.panel2.caption:=floattostr(dista*10000)+' � ';
    dist.panel6.caption:=floattostr(dista*10)+' mm ';
    dist.panel7.caption:=floattostr(dista*1)+' cm ';
    dist.panel10.caption:=floattostr(dista*0.01)+' m ';
    dist.panel8.caption:=floattostr(dista*1.00E-05)+' km ';
    dist.panel12.caption:=floattostr(dista*0.393700787)+' pol ';
    dist.panel13.caption:=floattostr(dista*0.032808399)+' p�s ';
    dist.panel14.caption:=floattostr(dista*1.09E-02)+' jardas ';
    dist.panel15.caption:=floattostr(dista*6.21E-06)+' mil ';
    dist.panel16.caption:=floattostr(dista*5.40E-06)+' mil ';
    dist.panel3.caption:=floattostr(dista*1.06E-18)+' al ';
    end;

if dist.radiobutton5.checked then begin
    dist.panel1.caption:=floattostr(dista*1.00E+10)+' � ';
    dist.panel2.caption:=floattostr(dista*1000000)+' � ';
    dist.panel6.caption:=floattostr(dista*1000)+' mm ';
    dist.panel7.caption:=floattostr(dista*100)+' cm ';
    dist.panel10.caption:=floattostr(dista*1)+' m ';
    dist.panel8.caption:=floattostr(dista*0.001)+' km ';
    dist.panel12.caption:=floattostr(dista*39.370078740)+' pol ';
    dist.panel13.caption:=floattostr(dista*3.2808398950)+' p�s ';
    dist.panel14.caption:=floattostr(dista*1.09E+00)+' jardas ';
    dist.panel15.caption:=floattostr(dista*6.21E-04)+' mil ';
    dist.panel16.caption:=floattostr(dista*0.000539957)+' mil ';
    dist.panel3.caption:=floattostr(dista*1.06E-16)+' al ';
    end;

if dist.radiobutton6.checked then begin
    dist.panel1.caption:=floattostr(1.00E+13*dista)+' � ';
    dist.panel2.caption:=floattostr(dista*1000000000)+' � ';
    dist.panel6.caption:=floattostr(dista*1000000)+' mm ';
    dist.panel7.caption:=floattostr(dista*100000)+' cm ';
    dist.panel10.caption:=floattostr(dista*1000)+' m ';
    dist.panel8.caption:=floattostr(dista*1)+' km ';
    dist.panel12.caption:=floattostr(dista*39370.078740)+' pol ';
    dist.panel13.caption:=floattostr(dista*3280.8398950)+' p�s ';
    dist.panel14.caption:=floattostr(dista*1.09E+03)+' jardas ';
    dist.panel15.caption:=floattostr(dista*6.21E-01)+' mil ';
    dist.panel16.caption:=floattostr(dista*0.539956804)+' mil ';
    dist.panel3.caption:=floattostr(dista*1.06E-13)+' al ';
    end;

if dist.radiobutton8.checked then begin
    dist.panel1.caption:=floattostr(254000000*dista)+' � ';
    dist.panel2.caption:=floattostr(dista*25400)+' � ';
    dist.panel6.caption:=floattostr(dista*25.4)+' mm ';
    dist.panel7.caption:=floattostr(dista*2.54)+' cm ';
    dist.panel10.caption:=floattostr(dista*0.0254)+' m ';
    dist.panel8.caption:=floattostr(dista*2.54E-05)+' km ';
    dist.panel12.caption:=floattostr(dista*1)+' pol ';
    dist.panel13.caption:=floattostr(dista*0.083333333)+' p�s ';
    dist.panel14.caption:=floattostr(dista*0.027777778)+' jardas ';
    dist.panel15.caption:=floattostr(dista*1.58E-08)+' mil ';
    dist.panel16.caption:=floattostr(dista*1.37E-05)+' mil ';
    dist.panel3.caption:=floattostr(dista*2.68E-18)+' al ';
    end;


if dist.radiobutton9.checked then begin
    dist.panel1.caption:=floattostr(dista*3048E06)+' � ';
    dist.panel2.caption:=floattostr(dista*304800)+' � ';
    dist.panel6.caption:=floattostr(dista*304.8)+' mm ';
    dist.panel7.caption:=floattostr(dista*30.48)+' cm ';
    dist.panel10.caption:=floattostr(dista*0.3048)+' m ';
    dist.panel8.caption:=floattostr(dista*0.0003048)+' km ';
    dist.panel12.caption:=floattostr(dista*12)+' pol ';
    dist.panel13.caption:=floattostr(dista*1)+' p�s ';
    dist.panel14.caption:=floattostr(dista*0.3333333)+' jardas ';
    dist.panel15.caption:=floattostr(dista*1.58E-05)+' mil ';
    dist.panel16.caption:=floattostr(dista*0.000164579)+' mil ';
    dist.panel3.caption:=floattostr(dista*3.22E-17)+' al ';
    end;

if dist.radiobutton10.checked then begin
    dist.panel1.caption:=floattostr(dista*9144E06)+' � ';
    dist.panel2.caption:=floattostr(dista*914400)+' � ';
    dist.panel6.caption:=floattostr(dista*914.4)+' mm ';
    dist.panel7.caption:=floattostr(dista*91.44)+' cm ';
    dist.panel10.caption:=floattostr(dista*0.9144)+' m ';
    dist.panel8.caption:=floattostr(dista*0.0009144)+' km ';
    dist.panel12.caption:=floattostr(dista*36)+' pol ';
    dist.panel13.caption:=floattostr(dista*3)+' p�s ';
    dist.panel14.caption:=floattostr(dista*1)+' jardas ';
    dist.panel15.caption:=floattostr(dista*1.89E-04)+' mil ';
    dist.panel16.caption:=floattostr(dista*0.000493737)+' mil ';
    dist.panel3.caption:=floattostr(dista*9.67E-17)+' al ';
    end;


if dist.radiobutton11.checked then begin
    dist.panel1.caption:=floattostr(dista*1.61E+13)+' � ';
    dist.panel2.caption:=floattostr(dista*1609344)+' � ';
    dist.panel6.caption:=floattostr(dista*1609344)+' mm ';
    dist.panel7.caption:=floattostr(dista*160934.4)+' cm ';
    dist.panel10.caption:=floattostr(dista*1609.344)+' m ';
    dist.panel8.caption:=floattostr(dista*1.609344)+' km ';
    dist.panel12.caption:=floattostr(dista*63360)+' pol ';
    dist.panel13.caption:=floattostr(dista*5280)+' p�s ';
    dist.panel14.caption:=floattostr(dista*1760)+' jardas ';
    dist.panel15.caption:=floattostr(dista*1)+' mil ';
    dist.panel16.caption:=floattostr(dista*0.868976242)+' mil ';
    dist.panel3.caption:=floattostr(dista*1.70E-13)+' al ';
    end;

if dist.radiobutton12.checked then begin
    dist.panel1.caption:=floattostr(dista*1.85E+13)+' � ';
    dist.panel2.caption:=floattostr(dista*1852000)+' � ';
    dist.panel6.caption:=floattostr(dista*1852000)+' mm ';
    dist.panel7.caption:=floattostr(dista*185200)+' cm ';
    dist.panel10.caption:=floattostr(dista*1852)+' m ';
    dist.panel8.caption:=floattostr(dista*1.852)+' km ';
    dist.panel12.caption:=floattostr(dista*72913.385830)+' pol ';
    dist.panel13.caption:=floattostr(dista*6076.1154860)+' p�s ';
    dist.panel14.caption:=floattostr(dista*2025.371829)+' jardas ';
    dist.panel15.caption:=floattostr(dista*1.150779448)+' mil ';
    dist.panel16.caption:=floattostr(dista*1)+' mil ';
    dist.panel3.caption:=floattostr(dista*1.96E-13)+' al ';
    end;

if dist.radiobutton13.checked then begin
    dist.panel1.caption:=floattostr(dista*9.46E+30)+' � ';
    dist.panel2.caption:=floattostr(dista*9.46E+18)+' � ';
    dist.panel6.caption:=floattostr(dista*9.46E+18)+' mm ';
    dist.panel7.caption:=floattostr(dista*9.46E+17)+' cm ';
    dist.panel10.caption:=floattostr(dista*9.46E+15)+' m ';
    dist.panel8.caption:=floattostr(dista*9.46E+12)+' km ';
    dist.panel12.caption:=floattostr(dista*3.72E+10)+' pol ';
    dist.panel13.caption:=floattostr(dista*3.10E+01)+' p�s ';
    dist.panel14.caption:=floattostr(dista*1.03E+01)+' jardas ';
    dist.panel15.caption:=floattostr(dista*5.88E+01)+' mil ';
    dist.panel16.caption:=floattostr(dista*5.108288337e+12)+' mil ';
    dist.panel3.caption:=floattostr(dista*1)+' al ';
    end;
end;


procedure Tdist.Edit1Exit(Sender: TObject);
begin
dista:=strtofloat(edit1.text);

atualiza;
end;







procedure Tdist.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton4Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton5Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton6Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton8Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton9Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton10Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton11Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton12Click(Sender: TObject);
begin
atualiza;
end;

procedure Tdist.RadioButton13Click(Sender: TObject);
begin
atualiza;
end;


procedure Tdist.Image1Click(Sender: TObject);
begin
dista:=dista-1;
edit1.text:=floattostr(dista);
atualiza;
end;

procedure Tdist.Image2Click(Sender: TObject);
begin
dista:=dista+1;
edit1.text:=floattostr(dista);
atualiza;

end;

end.
