program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Unit2 in 'Unit2.pas' {temp},
  Unit3 in 'Unit3.pas' {dist},
  Unit4 in 'Unit4.pas' {mass},
  Unit5 in 'Unit5.pas' {volume},
  Unit6 in 'Unit6.pas' {Forca},
  Unit7 in 'Unit7.pas' {energia},
  Unit8 in 'Unit8.pas' {tempo},
  Unit9 in 'Unit9.pas' {densidade},
  Unit10 in 'Unit10.pas' {velocidade},
  Unit11 in 'Unit11.pas' {pressao},
  Unit12 in 'Unit12.pas' {potencia},
  Unit13 in 'Unit13.pas' {area},
  Unit14 in 'Unit14.pas' {Form14},
  Unit15 in 'Unit15.pas' {AboutBox},
  Unit16 in 'Unit16.pas' {AboutBox1},
  Unit17 in 'Unit17.pas' {Form17};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Herveish arts Conversor';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(Ttemp, temp);
  Application.CreateForm(Tdist, dist);
  Application.CreateForm(Tmass, mass);
  Application.CreateForm(Tvolume, volume);
  Application.CreateForm(TForca, Forca);
  Application.CreateForm(Tenergia, energia);
  Application.CreateForm(Ttempo, tempo);
  Application.CreateForm(Tdensidade, densidade);
  Application.CreateForm(Tvelocidade, velocidade);
  Application.CreateForm(Tpressao, pressao);
  Application.CreateForm(Tpotencia, potencia);
  Application.CreateForm(Tarea, area);
  Application.CreateForm(TForm14, Form14);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TForm17, Form17);
  Application.Run;
end.
