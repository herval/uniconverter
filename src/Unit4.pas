unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  Tmass = class(TForm)
    Edit1: TEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    Panel4: TPanel;
    Panel5: TPanel;
    Image1: TImage;
    Image2: TImage;
    procedure Edit1Exit(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  mass: Tmass;
  massa:real;

implementation

{$R *.DFM}

procedure atualiza;
begin
if mass.radiobutton1.checked then begin
   mass.panel1.caption:=floattostr(massa*1)+' mg ';
   mass.panel2.caption:=floattostr(massa*0.001)+' g ';
   mass.panel3.caption:=floattostr(massa*1.00E-06)+' kg ';
   mass.panel4.caption:=floattostr(massa*2.20E-06)+' lb ';
   mass.panel5.caption:=floattostr(massa*1.10E-09)+' ton ';
   end;

if mass.radiobutton2.checked then begin
   mass.panel1.caption:=floattostr(massa*1000)+' mg ';
   mass.panel2.caption:=floattostr(massa*1)+' g ';
   mass.panel3.caption:=floattostr(massa*0.001)+' kg ';
   mass.panel4.caption:=floattostr(massa*2.20E-03)+' lb ';
   mass.panel5.caption:=floattostr(massa*1.10E-06)+' ton ';
   end;

if mass.radiobutton3.checked then begin
   mass.panel1.caption:=floattostr(massa*1000000)+' mg ';
   mass.panel2.caption:=floattostr(massa*1000)+' g ';
   mass.panel3.caption:=floattostr(massa*1)+' kg ';
   mass.panel4.caption:=floattostr(massa*2.204622622)+' lb ';
   mass.panel5.caption:=floattostr(massa*1.10E-03)+' ton ';
   end;

if mass.radiobutton4.checked then begin
   mass.panel1.caption:=floattostr(massa*453592.37)+' mg ';
   mass.panel2.caption:=floattostr(massa*453.59237)+' g ';
   mass.panel3.caption:=floattostr(massa*0.45359237)+' kg ';
   mass.panel4.caption:=floattostr(massa*1)+' lb ';
   mass.panel5.caption:=floattostr(massa*0.0005)+' ton ';
   end;

if mass.radiobutton5.checked then begin
   mass.panel1.caption:=floattostr(massa*907184739.9)+' mg ';
   mass.panel2.caption:=floattostr(massa*907184.7399)+' g ';
   mass.panel3.caption:=floattostr(massa*907.1847399)+' kg ';
   mass.panel4.caption:=floattostr(massa*2000)+' lb ';
   mass.panel5.caption:=floattostr(massa*1)+' ton ';
   end;

end;



procedure Tmass.Edit1Exit(Sender: TObject);
begin
massa:=strtofloat(edit1.text);

atualiza;
end;

procedure Tmass.RadioButton1Click(Sender: TObject);
begin
atualiza;
end;

procedure Tmass.RadioButton2Click(Sender: TObject);
begin
atualiza;
end;

procedure Tmass.RadioButton3Click(Sender: TObject);
begin
atualiza;
end;

procedure Tmass.RadioButton4Click(Sender: TObject);
begin
atualiza;
end;

procedure Tmass.RadioButton5Click(Sender: TObject);
begin
atualiza;
end;

procedure Tmass.Image1Click(Sender: TObject);
begin
massa:=massa-1;
edit1.text:=floattostr(massa);
atualiza;

end;


procedure Tmass.Image2Click(Sender: TObject);
begin
massa:=massa+1;
edit1.text:=floattostr(massa);
atualiza;

end;

end.
