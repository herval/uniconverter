unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Buttons, DBCtrls, ExtCtrls, Menus;

type
  TForm1 = class(TForm)
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn1: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    Bevel1: TBevel;
    Label1: TLabel;
    MainMenu1: TMainMenu;
    Converso1: TMenuItem;
    Temperatura1: TMenuItem;
    Distncia1: TMenuItem;
    Massa1: TMenuItem;
    Volume1: TMenuItem;
    rea1: TMenuItem;
    Fora1: TMenuItem;
    Potncia1: TMenuItem;
    Energia1: TMenuItem;
    Presso1: TMenuItem;
    Tempo1: TMenuItem;
    Densidade1: TMenuItem;
    Velocidade1: TMenuItem;
    Sobre1: TMenuItem;
    Help1: TMenuItem;
    Sair1: TMenuItem;
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Temperatura1Click(Sender: TObject);
    procedure Distncia1Click(Sender: TObject);
    procedure Massa1Click(Sender: TObject);
    procedure Volume1Click(Sender: TObject);
    procedure rea1Click(Sender: TObject);
    procedure Fora1Click(Sender: TObject);
    procedure Potncia1Click(Sender: TObject);
    procedure Energia1Click(Sender: TObject);
    procedure Presso1Click(Sender: TObject);
    procedure Tempo1Click(Sender: TObject);
    procedure Densidade1Click(Sender: TObject);
    procedure Velocidade1Click(Sender: TObject);
    procedure Sobre1Click(Sender: TObject);
    procedure Help1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Unit2, Unit3, Unit4, Unit5, Unit6, Unit7, Unit8, Unit9, Unit10, Unit11,
  Unit12, Unit13, Unit15, Unit16, Unit17;

{$R *.DFM}






procedure TForm1.BitBtn11Click(Sender: TObject);
begin
temp.show;
end;

procedure TForm1.BitBtn7Click(Sender: TObject);
begin
dist.show;
end;

procedure TForm1.BitBtn8Click(Sender: TObject);
begin
mass.show;
end;

procedure TForm1.BitBtn13Click(Sender: TObject);
begin
volume.show;
end;

procedure TForm1.BitBtn9Click(Sender: TObject);
begin
forca.show;
end;

procedure TForm1.BitBtn6Click(Sender: TObject);
begin
energia.show;
end;

procedure TForm1.BitBtn10Click(Sender: TObject);
begin
tempo.show;
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
begin
densidade.show;
end;

procedure TForm1.BitBtn12Click(Sender: TObject);
begin
velocidade.show;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
begin
pressao.show;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
potencia.show;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
area.show;
end;

procedure TForm1.Temperatura1Click(Sender: TObject);
begin
temp.show;
end;

procedure TForm1.Distncia1Click(Sender: TObject);
begin
dist.show;
end;

procedure TForm1.Massa1Click(Sender: TObject);
begin
mass.show;
end;

procedure TForm1.Volume1Click(Sender: TObject);
begin
volume.show;
end;

procedure TForm1.rea1Click(Sender: TObject);
begin
area.show;
end;

procedure TForm1.Fora1Click(Sender: TObject);
begin
forca.show;
end;

procedure TForm1.Potncia1Click(Sender: TObject);
begin
potencia.show;
end;

procedure TForm1.Energia1Click(Sender: TObject);
begin
energia.show;
end;

procedure TForm1.Presso1Click(Sender: TObject);
begin
pressao.show;
end;

procedure TForm1.Tempo1Click(Sender: TObject);
begin
tempo.show;
end;

procedure TForm1.Densidade1Click(Sender: TObject);
begin
densidade.show;
end;

procedure TForm1.Velocidade1Click(Sender: TObject);
begin
velocidade.show;
end;

procedure TForm1.Sobre1Click(Sender: TObject);
begin
aboutbox.show;
end;

procedure TForm1.Help1Click(Sender: TObject);
begin
form17.show;
end;

procedure TForm1.Sair1Click(Sender: TObject);
begin
form1.close;
end;

end.
